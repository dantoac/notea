# coding: utf8


class Notea:

    def __init__(self, user_email=None):
        self.user_email = user_email
        return None

    def get(self, context=None):
        '''
        Devuelve un dataset con las notificaciones de un usuario.
        Opcionalmente, filtra segun el parametro "context" si este esta definido.

        '''

        if context:
            dataset = db((db.notea_notification.notea_context == context)
                         & (self.user_email in db.notea_notification.notea_target)
                         & (db.notea_notification.notea_consumed == False)
                     ).select(db.notea_notification.notea_uuid,
                              db.notea_notification.notea_created_on,
                              db.notea_notification.notea_context,
                              db.notea_notification.notea_title,
                              db.notea_notification.notea_body,
                     )
        else:

            dataset = db((db.notea_notification.notea_target.contains(self.user_email))
                         & (db.notea_notification.notea_consumed == False)
                     ).select(db.notea_notification.notea_uuid,
                              db.notea_notification.notea_created_on,
                              db.notea_notification.notea_context,
                              db.notea_notification.notea_title,
                              db.notea_notification.notea_body,
                     )

        return dataset

    def consume(self, notea_uuid=None):
        '''
        Consume una notificacion por un usuario.
        Devuelve la cantidad de filas afectadas por el update.
        '''
        dataset = db(db.notea_notification.notea_uuid == notea_uuid).update(
            notea_consumed = True,
            notea_consumed_on = request.now.now(),
            notea_consumed_by = self.user_email,
            notea_consumed_from = request.env.request_uri
        )

        return dataset

'''
Campos:

* NOTEA_CONTEXT: permite agregar las notificaciones realizadas en un contexto especifica
* NOTEA_TARGET: originalmente, se desnormalizara la referencia a usuarios del sistema utilizando una lista de los respectivos correos de usuario como destinos de la notificacion.
* NOTEA_CREATED_ON: timestamp de cuando se crea la notificacion
* NOTEA_CONSUMED_ON: timestamp de cuando la notificacion ha sido confirmada por algun usuario destino.
* NOTEA_CONSUMED_BY: email del usuario destino que primero confirmo la notificacion
* NOTEA_CONSUMED_FROM: URL de la aplicacion desde la que se consumio la notificacion
* NOTEA_TITLE: titulo de la notificacion (opcional)
* NOTEA_BODY: mensaje de la notificacion. (obligatorio)
'''

import uuid

db.define_table('notea_notification',
                Field('notea_uuid', default=uuid.uuid4()),
                Field('notea_context'), 
                Field('notea_consumed', 'boolean', default=False),
                Field('notea_target','list:string'),
                Field('notea_created_on', 'datetime', default=request.now),
                Field('notea_created_by', requires=IS_EMPTY_OR(IS_EMAIL())),
                Field('notea_consumed_on', 'datetime'),
                Field('notea_consumed_by', requires=IS_EMPTY_OR(IS_EMAIL())),
                Field('notea_consumed_from', requires=IS_URL()),
                Field('notea_title'),
                Field('notea_body', requires=IS_NOT_EMPTY()),
                )


NOTEA = Notea(auth.user.email) if auth.is_logged_in() else None


